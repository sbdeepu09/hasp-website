<H2ck3r_5p2c3>
#What && !What
def TheWhat(club)
{
  We are a group of weird guys who doesn't seriously care about our mark-sheets but the things we actually implement. The marks highlighted on our answer sheets doesn't turn us on, but the stars on our GitHub project gives us goosebumps.
}
def The!What(club)
{
  We are not the cliche club people who conduct programms to enter it in report or arrange meetings to fill the minutes book. We don't believe in reports and paper-works but the actual work done to solve real-life problems.
}
main()
{
  TheWhat(Hacker_Space);
  The!What(Hacker_Space);
  If you do something amazing one day, it can be a coincidence. 
  If it happens twice, it can be pressure or infatuation. 
  But if you are consistent, then it becomes a culture. We intend to cultivate that culture among ourselves.
  The realization that your immature code can really make a difference is powerful. We are the magicians who brings you the solutions to your problems with the touch of a finger. If you are foolish enough to believe that you can change the world with your code, then you are one of us. We usher you all to be part of this revolution.
  #Keep Calm & Keep Coding
}